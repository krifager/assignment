<?php
/** The Model implementation of the IMT2571 Assignment #1 MVC-example, storing
  * data in a MySQL database using PDO.
  * @author Rune Hjelsvold
  * @see http://php-html.net/tutorials/model-view-controller-in-php/
  *      The tutorial code used as basis.
  */

require_once("AbstractEventModel.php");
require_once("Event.php");
require_once("dbParam.php");

/** The Model is the class holding data about a archive of events.
  * @todo implement class functionality.
  */
class DBEventModel extends AbstractEventModel
{
    protected $db = null;

    public function __construct($db = null)
    {
        if ($db) {                                          //if already connected
            $this->db = $db;
        } else {                                            //if not connected
            try{
              $this->db = new PDO('mysql:host=' .DB_HOST. ';dbname='.DB_NAME.';charset=utf8mb4;', DB_USER, DB_PWD,
                    array(PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION));  //create connection to DB
            } catch (\PDOException $e){
              throw new \PDOException($e->getMessage());
            }
        }
    }

    public function getEventArchive()
    {
        $eventList = array();
      try{
        $stmt = $this->db->query('SELECT * FROM event ORDER BY id');

        while($row = $stmt->fetch(PDO::FETCH_ASSOC)){
          $eventList[] = new Event($row['title'], $row['date'],$row['description'],$row['id']);
        }
      //  print_r($eventList);
        return $eventList;

      } catch(\PDOException $e){
        throw new \PDOException($e->getMessage());
      }
    }

    /** Function retrieving information about a given event in the archive.
      * @param integer $id the id of the event to be retrieved
      * @return Event|null The event matching the $id exists in the archive;
      *         null otherwise.
      * @todo Implement function using PDO and a real database.
      * @throws PDOException
      */
    public function getEventById($id)
    {
        $this->invalidId($id);
        $event = null;

        try{
          $stmt = $this->db->prepare("SELECT * FROM event WHERE id = $id");  //select all data from event where id = $id
          $stmt->bindValue(':id', $id);                                                                 //bind value to id
          $stmt->execute();                                                                             //execute query that returns true or false
          $row = $stmt->fetch(PDO::FETCH_ASSOC);
          $event = new Event($row['title'], $row['date'], $row['description'], $row['id']);                                                                    //get associative array
          return $event;                                                                               //return event
      } catch(\PDOException $e){
        throw new \PDOException($e->getMessage());
      }
    }

    /** Adds a new event to the archive.
      * @param Event $event The event to be added - the id of the event will be set after successful insertion.
      * @todo Implement function using PDO and a real database.
      * @throws PDOException
      * @throws InvalidArgumentException If event data is invalid
      */
    public function addEvent($event)          //event is an object?
    {
      try{

        $event->verify();
        $stmt = $this->db->prepare('INSERT INTO event(title, date, description) ' //set up query and bind values later
                            . 'VALUES(:title, :date, :description)');

        $stmt->bindValue(':title', $event->title);                               //give values to the query
        $stmt->bindValue(':date', $event->date);
        $stmt->bindValue(':description', $event->description);

        $stmt->execute();                                           //execute query. True or false
        $event->id = $this->db->lastInsertId();
      } catch(\PDOException $e){
        throw new \PDOException($e->getMessage());
      } catch(\InvalidArgumentException $e){
        throw new \InvalidArgumentException($e->getMessage());
      }
    }

    /** Modifies data related to a event in the archive.
      * @param Event $event The event data to be kept.
      * @todo Implement function using PDO and a real database.
      * @throws PDOException
      * @throws InvalidArgumentException If event data is invalid
     */
    public function modifyEvent($event)
    {
      try{
        $event->verify();

        $stmt = $this->db->prepare("UPDATE event SET title = ?, date = ?, description = ? WHERE id = ?");  //query

        $title = $event->title;
        $date = $event->date;
        $description = $event->description;
        $id = $event->id;

        $stmt->execute([$title, $date, $description, $id]);

      }catch(\PDOException $e){
        throw new \PDOException($e->getMessage());
      } catch(\InvalidArgumentException $e){
        throw new \InvalidArgumentException($e->getMessage());
      }
    }

    /** Deletes data related to a event from the archive.
      * @param $id integer The id of the event that should be removed from the archive.
      * @todo Implement function using PDO and a real database.
      * @throws PDOException
     */
    public function deleteEvent($id)
    {
        try{
          $this->invalidId($id);

          $stmt = $this->db->prepare("DELETE FROM event WHERE id = $id");
          $stmt->execute();
        } catch(\PDOException $e){
          throw new \PDOException($e->getMessage());
    }
  }
  public const MSG_TABLE = ['Event id expected to be a valid number. ',
                          'Event title is mandatory. ',
                          'Event date is mandatory. ',
                          'Date must be of format YYYY-MM-DD. ',
                          'Date must be an existing one. '
                      ];

  public function invalidId($id){
    if(!is_numeric($id))
      throw new InvalidArgumentException(self::MSG_TABLE[0]);
  }
}
